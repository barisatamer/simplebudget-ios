//
//  AppDelegate.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 31/01/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import GoogleSignIn
import SwiftyBeaver
import IQKeyboardManagerSwift

// SwiftyBeaver logger
let log = SwiftyBeaver.self

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupSwiftyBeaver()
        initializeGoogleSignIn()
        try! setupDatabase(application)
        
        let showLogin = false
        if showLogin {
            window?.rootViewController = UINavigationController(rootViewController: Storyboard.Login.instantiate(LoginViewController.self))
        } else {
            window?.rootViewController = UINavigationController(rootViewController: Storyboard.Main.instantiate(MainViewController.self))
        }
        window?.makeKeyAndVisible()
        
        // Enable keyboard tracking
        IQKeyboardManager.sharedManager().enable = true
        
        return true
    }
    
    func initializeGoogleSignIn() {
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "981264209340-fai11irscn0m86q3vp9gnl1im4ukgapk.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            print("👨🏻‍⚕️ userId: \(userId!)")
            print("#️⃣ idToken: \(idToken!)")
            print("fullName: \(fullName!)")
            print("givenName: \(givenName!)")
            print("familyName: \(familyName!)")
            print("email: \(email!)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        // Google Sign in
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation] as? String)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

