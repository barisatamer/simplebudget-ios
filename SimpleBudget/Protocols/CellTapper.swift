//
//  CellTapper.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 10/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation

protocol CellTapper {
    func didTappedCell(withTransaction transactionModel: TransactionModel)
}
