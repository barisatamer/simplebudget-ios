//
//  DefaultValues.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 26/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

enum DefaultValues {
    enum CollectionView {
        static let LeftPadding: CGFloat = 15
        static let RightPadding: CGFloat = 15
    }
    enum CategoryCollectionView {
        static let ItemWidth: CGFloat = 90
    }
}
