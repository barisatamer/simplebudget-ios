//
//  SwiftBeaverConfiguration.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import SwiftyBeaver

// For usage info and configuration, see: https://github.com/SwiftyBeaver/SwiftyBeaver#usage
func setupSwiftyBeaver() {
    let console = ConsoleDestination()  // log to Xcode Console
    console.format = "$DHH:mm:ss$d $L $M"
    
    log.addDestination(console)
}
