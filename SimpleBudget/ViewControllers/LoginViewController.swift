//
//  LoginViewController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 01/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    @IBAction func loginButtonDidTapped(_ sender: Any) {
        navigationController?.pushViewController(Storyboard.Main.instantiate(MainViewController.self),
                                                 animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
