//
//  SettingsViewController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 06/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction func closeButtonDidTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
