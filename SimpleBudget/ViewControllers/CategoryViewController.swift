//
//  CategoryViewController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 18/03/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import IGListKit
import RxSwift
import GRDB

class CategoryViewController: UIViewController, RxDisposable, CategorySelectionDelegate {

    weak var delegate: CategorySelectionDelegate?
    
    // MARK: - IB Outlets
    
    @IBOutlet weak var categorySelectorView: CategorySelectionView!
    
    
    // MARK: - Private Properties
    
    var disposeBag = DisposeBag()
    fileprivate var categories = [ListDiffable]()
    
    
    // MARK: - IGListKit
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(),
                           viewController: self,
                           workingRangeSize: 0)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        removeBackButtonTitle()
        // TODO: Lokalize
        self.title = "Categories"
        
        disposeBag = DisposeBag()
        Category.all().subscribe(self, onNext: { (results) in
            self.didReceivedCategories(results)
        })
        
        // Set adapter & data source
        adapter.collectionView = self.categorySelectorView.collectionView
        adapter.dataSource = self
    }
    
    func didReceivedCategories(_ results: [CategoryViewModel]) {
        categories.removeAll()
        // TODO: Lokalize 'New'
        categories.append(CategoryViewModel(categoryModel: CategoryModel(name: "New..", icon: "➕", id: -1)))
        results.forEach { categories.append($0) }
        adapter.performUpdates(animated: true, completion: nil)
    }
    
    func categoryDidSelected(_ category: CategoryModel) {
        if category.id == -1 {
            addCategoryCellDidTapped()
        } else {
            delegate?.categoryDidSelected(category)
            navigationController?.popViewController(animated: true)
        }
    }
    
    func addCategoryCellDidTapped() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddCategoryViewController") as! AddCategoryViewController
        navigationController?.pushViewController(vc, animated: true)
    }
}


// MARK: - ListAdapterDataSource

extension CategoryViewController: ListAdapterDataSource {
    
    public func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return self.categories
    }
    
    public func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let sectionController = CategorySectionController()
        return sectionController
    }
    
    public func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }
}
