//
//  AddCategoryViewController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import ViewAnimator

class AddCategoryViewController: UIViewController {

    
    // MARK: - IB Outlets
    
    @IBOutlet weak var textFieldName: TextField!
    @IBOutlet weak var textFieldIcon: TextField!
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: - Private Properties
    
    let category: CategoryModel = CategoryModel(name: "", icon: "")
    private let fromAnimation = AnimationType.from(direction: .right, offset: 130.0)
    
    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        removeBackButtonTitle()
        
        // TODO: Lokalize
        title = "Add Category"
        
        textFieldName.placeholder = "Category name"
        textFieldIcon.placeholder = "Icon"
        
        UIView.animate(views: stackView.arrangedSubviews, animations: [fromAnimation], reversed: false,
                       initialAlpha: 0.0, finalAlpha: 1.0, delay: 0.01)
    }
    
    
    // MARK: - Button events
    
    @IBAction func save(_ sender: Any) {
        category.name = textFieldName.text!
        category.icon = textFieldIcon.text!
        
        do {
            try Category.save(category)
            self.navigationController?.popViewController(animated: true)
        } catch {
            log.debug("Eror \(error)")
        }
    }
    
}
