//
//  MainViewController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 02/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import IGListKit
import Charts
import RxSwift
import GRDB
import ParallaxScrollView

class MainViewController: UIViewController, ChartViewDelegate, RxDisposable {
    
    // MARK: - Private Properties
    
    var disposeBag = DisposeBag()
    fileprivate var transactions = [ListDiffable]()
    fileprivate var selectedDate: Date?
    
    fileprivate var scrollingAnimationInProgress = false
    
    fileprivate var currentYear = Date().startOfTheYear() {
        didSet {
            getTransactions()
        }
    }
    fileprivate var currentMonth: Int = 1
    
    
    // MARK: - IB Outlets
    
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var headerView: MainHeaderView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewTopConstraint: NSLayoutConstraint!
    
    // MARK: - IGListKit
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(),
                           viewController: self,
                           workingRangeSize: 0)
    }()
    let collectionView: UICollectionView = {
        let layout = ListCollectionViewLayout.init(stickyHeaders: false, scrollDirection: UICollectionViewScrollDirection.horizontal, topContentInset: 0, stretchToEdge: true)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return collectionView
    }()
    
    
    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        addConstrainsts()
        
        adapter.collectionView = collectionView
        adapter.dataSource = self
        adapter.scrollViewDelegate = self
        
        view.bringSubview(toFront: buttonAdd)
        
        getTransactions()
        
        automaticallyAdjustsScrollViewInsets = false
        view.bringSubview(toFront: headerView)
        headerView.monthsParallaxScrollView.delegate = self
        headerView.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func getTransactions() {
        disposeBag = DisposeBag()
        
        Transaction.getTransactions(forYear: currentYear).subscribe(self, onNext: { (results) in
            self.updateList(results: results)
        })
    }
    
    func updateList(results: [FeedItem]) {
        transactions.removeAll()
        results.forEach { transactions.append($0) }
        adapter.performUpdates(animated: true, completion: nil)
    }
    
    private func addConstrainsts() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        // TODO: Refactor those lines
        if #available(iOS 11.0, *) {
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 66).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            collectionView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
            collectionView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        } else {
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 86).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func addButtonDidTapped(_ sender: Any) {
        
        if let transactionEditor = self.storyboard?.instantiateViewController(withIdentifier: "AddTransactionViewController") as? AddTransactionViewController {

            // Prepare the date
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            
            // Create a date for the selected page.. Refactor this. .
            if let date = formatter.date(from: "\(currentMonth)/28/\(currentYear.stringWithYear())"),
                date.startOfMonth() != Date().startOfMonth() {
                print("Date \(date)")
                transactionEditor.date = date
            }
            
            transactionEditor.presentByEmbeddingInNavigationBar(animated: true, from: self)
        }
    }
    
}

// MARK: - ListAdapterDataSource

extension MainViewController: ListAdapterDataSource {
    
    public func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return transactions
    }
    
    public func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return FullScreenHoriztonalSectionController()
    }
    
    public func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }
}


// MARK: - Cell Tapper

extension MainViewController: CellTapper {
    
    func didTappedCell(withTransaction transactionModel: TransactionModel) {
        if let transactionEditor = self.storyboard?.instantiateViewController(withIdentifier: "AddTransactionViewController") as? AddTransactionViewController {
            transactionEditor.transaction = transactionModel
            transactionEditor.presentByEmbeddingInNavigationBar(animated: true, from: self)
        }
    }
    
}

extension MainViewController: FullScreenHorizontalSectionDelegate {
    
    func didScrolledTo(index: Int) {

        if let item = transactions[index] as? FeedItem {
            // print("Scrolled to item : \(index) \(item.headerTitle)")
//            headerView.titleLabel.text = item.headerTitle
        }
    }
}

extension MainViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollingAnimationInProgress = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !scrollingAnimationInProgress {
            headerView.monthsParallaxScrollView.setContentOffset(offset: CGPoint(x: scrollView.contentOffset.x, y: 0))
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollingAnimationInProgress = false
        headerView.monthsParallaxScrollView.scrollViewDidEndDecelerating(scrollView)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollingAnimationInProgress = false
    }
}


extension MainViewController: ParallaxScrollViewDelegate {
    // TODO: Refactor this, move into HeaderView
    func pageIndexDidChanged(index: Int) {
        scrollingAnimationInProgress = true
        adapter.scroll(to: transactions[index], supplementaryKinds: nil, scrollDirection: .horizontal, scrollPosition: .centeredHorizontally, animated: true)
        self.currentMonth = index + 1
    }
}

extension MainViewController: HeaderViewDelegate {
    func didPicked(year: Date) {
        currentYear = year
        adapter.scroll(to: transactions[0], supplementaryKinds: nil, scrollDirection: .horizontal, scrollPosition: .centeredHorizontally, animated: true)
    }
}


// MARK: - ScrollDelegate, used for vertical scrolling

var touched = false
extension MainViewController: ScrollDelegate {
    
    func didScroll(offset: CGPoint) {
        
        if offset.y < 0 && touched {
            headerView.setHeightConstraint(constant: 66 - offset.y)
        }
    }
    
    func willBeginDragging() {
        touched = true
    }
    
    func didEndDragging() {
        touched = false
        headerView.endDragging()
    }
}
