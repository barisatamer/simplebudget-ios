//
//  AddTransactionViewController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import IGListKit
import RxSwift
import GRDB
import ViewAnimator

class AddTransactionViewController: UIViewController, RxDisposable, CategorySelectionDelegate {
    
    
    // MARK: - IB Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textFieldAmount: UITextField!
    @IBOutlet weak var textFieldName: TextField!
    @IBOutlet weak var textFieldDescription: TextField!
    @IBOutlet weak var textFieldCategory: TextField!
    @IBOutlet weak var segmentedControlTransactionType: UISegmentedControl!
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: - Private Properties
    
    private let fromAnimation = AnimationType.from(direction: .bottom, offset: 130.0)
    
    var disposeBag = DisposeBag()
    var date = Date()
    lazy var transaction: TransactionModel = TransactionModel(name: "",
                                                         description: "",
                                                         currency: "eur",
                                                         amount: 0,
                                                         categoryId: 1,
                                                         createdAt: self.date )
    
    
    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(AddTransactionViewController.dismissKeyboard))
        recognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(recognizer)
        
        // Set the variables
        textFieldAmount.text = String(abs(transaction.amount))
        textFieldName.text = transaction.name
        textFieldDescription.text = transaction.desc
        textFieldCategory.text = transaction.categoryName
        segmentedControlTransactionType.selectedSegmentIndex = transaction.amount < 0 ? 0 : 1
        if transaction.id != nil {
            self.title = "Edit Transaction"
        }

        // TODO: Also change text fields' color
        if transaction.amount < 0 {
            navigationController?.navigationBar.barTintColor = ColorPalette.Custom.RedLight
            segmentedControlTransactionType.tintColor = ColorPalette.Custom.RedLight
        }
        
        UIView.animate(views: stackView.arrangedSubviews, animations: [fromAnimation], reversed: false,
                       initialAlpha: 0.0, finalAlpha: 1.0, delay: 0.01)
    }

    func initNavigationBar() {
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.close(_:)))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(self.save(_:)))
        self.navigationItem.rightBarButtonItem = saveButton
    }
    
    
    // MARK: - Navigation button events
    
    @objc func close(_ sender: UIBarButtonItem) {
        UIView.animate(views: stackView.arrangedSubviews.reversed(), animations: [fromAnimation], reversed: true,
                       initialAlpha: 1.0, finalAlpha: 0.0, delay: 0.05)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400), execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @objc func save(_ sender: UIBarButtonItem) {
        transaction.name = textFieldName.text!
        
        if let amount = Double(textFieldAmount.text!) {
            transaction.amount = amount
            if segmentedControlTransactionType.selectedSegmentIndex == 0 {
                transaction.amount *= -1
            }
        }
        
        transaction.desc = textFieldDescription.text!
        
        do {
            try Transaction.save(transaction: transaction)
            close(sender)
        } catch {
            log.debug("Eror \(error)")
        }
    }
    
    // MARK: - Action
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func categoryFieldDidTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func categoryDidSelected(_ category: CategoryModel) {
        transaction.categoryId = category.id!
        textFieldCategory.text = category.name
    }
}
