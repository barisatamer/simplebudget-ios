//
//  DateSelectorViewController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 07/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import FSCalendar

class DateSelectorViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {

    
    // MARK: - Private properties
    
    fileprivate lazy var dateFormatterSQL: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    fileprivate var selectedDate: Date?
    fileprivate var selectedDates: [Date]?
    fileprivate var selectedDateTextRepresentation: String?
    
    // MARK: - Public properties
    
    var onDateSelected: ((Date, String) -> ())?
    var onDatesSelected: (([Date], String) -> ())?
    
    // MARK: - IB Outlets
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var buttonSelectMonth: UIButton!
    @IBOutlet weak var buttonSelectInterval: UIButton!
    
    
    // MARK: - View Cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()

        calendar.delegate = self
        calendar.allowsMultipleSelection = true
        
        selectedDate = calendar.currentPage.startOfMonth()
        selectedDateTextRepresentation = calendar.currentPage.monthName()
    }
    
    
    // MARK: - Calendar
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectedDateTextRepresentation = calendar.selectedDates.count > 1 ? "dates" : " date"
        self.buttonSelectInterval.setTitle("Select \(selectedDateTextRepresentation ?? "")", for: .normal)
        self.selectedDates = calendar.selectedDates.map { $0.withoutTime() }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        selectedDateTextRepresentation = calendar.currentPage.monthName()
        self.buttonSelectMonth.setTitle("Select \(selectedDateTextRepresentation ?? "")", for: .normal)
        selectedDate = calendar.currentPage.startOfMonth()
    }
    
    
    // MARK: - Buttons
    
    @IBAction func closeButtonDidTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectMonthButtonDidTapped(_ sender: Any) {
        guard let closure = onDateSelected,
            let date = selectedDate,
            let text = selectedDateTextRepresentation
            else {
                dismiss(animated: true, completion: nil)
                return
        }
        
        closure(date, text)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectDatesButtonDidTapped(_ sender: Any) {
        guard let closure = onDatesSelected,
            let dates = selectedDates
             else { return }
        
        closure(dates, dates.count > 1 ? "Multiple days" : "Selected day")
        dismiss(animated: true, completion: nil)
    }
}
