//
//  ColorPalette.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 20/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

enum ColorPalette {
    
    enum FlatUI {
        static let Turquoise = #colorLiteral(red: 0.1019607843, green: 0.737254902, blue: 0.6117647059, alpha: 1) // 1ABC9C
        static let GreenSea = #colorLiteral(red: 0.0862745098, green: 0.6274509804, blue: 0.5215686275, alpha: 1) // 16A085
        static let Emerald = #colorLiteral(red: 0.1803921569, green: 0.8, blue: 0.4431372549, alpha: 1) // 2ECC71
        static let Nephritis = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1) // #27ae60
        static let PeterRiver = #colorLiteral(red: 0.2039215686, green: 0.5960784314, blue: 0.8588235294, alpha: 1) // #3498db
        static let BelizeHole = #colorLiteral(red: 0.1607843137, green: 0.5019607843, blue: 0.7254901961, alpha: 1) // #2980b9
        static let Amethyst = #colorLiteral(red: 0.6078431373, green: 0.3490196078, blue: 0.7137254902, alpha: 1) // #9b59b6
        static let Wisteria = #colorLiteral(red: 0.5568627451, green: 0.2666666667, blue: 0.6784313725, alpha: 1) // #8e44ad
        static let WetAsphalt = #colorLiteral(red: 0.2039215686, green: 0.2862745098, blue: 0.368627451, alpha: 1) // #34495e
        static let MidnightBlue = #colorLiteral(red: 0.1725490196, green: 0.2431372549, blue: 0.3137254902, alpha: 1) // #2c3e50
        static let SunFlower = #colorLiteral(red: 0.9450980392, green: 0.768627451, blue: 0.05882352941, alpha: 1) // #f1c40f
        static let Orange = #colorLiteral(red: 0.9529411765, green: 0.6117647059, blue: 0.07058823529, alpha: 1) // #f39c12
        static let Carrot = #colorLiteral(red: 0.9019607843, green: 0.4941176471, blue: 0.1333333333, alpha: 1) // #e67e22
        static let Pumpkin = #colorLiteral(red: 0.8274509804, green: 0.3294117647, blue: 0, alpha: 1) // #d35400
        static let Alizarin = #colorLiteral(red: 0.9058823529, green: 0.2980392157, blue: 0.2352941176, alpha: 1) // #e74c3c
        static let Pomegranate = #colorLiteral(red: 0.7529411765, green: 0.2235294118, blue: 0.168627451, alpha: 1) // #c0392b
        static let Clouds = #colorLiteral(red: 0.9254901961, green: 0.9411764706, blue: 0.9450980392, alpha: 1) // #ecf0f1
        static let Silver = #colorLiteral(red: 0.7411764706, green: 0.7647058824, blue: 0.7803921569, alpha: 1) // #bdc3c7
        static let Concrete = #colorLiteral(red: 0.5843137255, green: 0.6470588235, blue: 0.6509803922, alpha: 1) // #95a5a6
        static let Asbestos = #colorLiteral(red: 0.4980392157, green: 0.5490196078, blue: 0.5529411765, alpha: 1) // #7f8c8d
        
        
        static let FlatColors = [ColorPalette.FlatUI.Alizarin,
                                 ColorPalette.FlatUI.Amethyst,
                                 ColorPalette.FlatUI.Asbestos,
                                 ColorPalette.FlatUI.BelizeHole,
                                 ColorPalette.FlatUI.Carrot,
                                 ColorPalette.FlatUI.Clouds,
                                 ColorPalette.FlatUI.Concrete,
                                 ColorPalette.FlatUI.Emerald,
                                 ColorPalette.FlatUI.GreenSea,
                                 ColorPalette.FlatUI.MidnightBlue,
                                 ColorPalette.FlatUI.Nephritis,
                                 ColorPalette.FlatUI.Orange,
                                 ColorPalette.FlatUI.PeterRiver,
                                 ColorPalette.FlatUI.Pomegranate,
                                 ColorPalette.FlatUI.Pumpkin,
                                 ColorPalette.FlatUI.Silver,
                                 ColorPalette.FlatUI.SunFlower,
                                 ColorPalette.FlatUI.Turquoise,
                                 ColorPalette.FlatUI.WetAsphalt,
                                 ColorPalette.FlatUI.Wisteria
        ]
    }
    
    enum Custom {
        static let Green = #colorLiteral(red: 0.09019607843, green: 0.6274509804, blue: 0.5215686275, alpha: 1) // 17A085
        static let GreenLighter = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3725490196, alpha: 1) // 27AE5F
        static let BlueDark = #colorLiteral(red: 0.1529411765, green: 0.231372549, blue: 0.3098039216, alpha: 1) // 273B4F
        static let BlueLight = #colorLiteral(red: 0.2156862745, green: 0.3019607843, blue: 0.3843137255, alpha: 1) // 374D62
        static let BlueLighter = #colorLiteral(red: 0.3098039216, green: 0.3725490196, blue: 0.4392156863, alpha: 1) // 4F5F70
        static let Gray = #colorLiteral(red: 0.7137254902, green: 0.7137254902, blue: 0.7137254902, alpha: 1) // B6B6B6
        static let GrayLight = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1) // C8C8C8
        static let RedLight = #colorLiteral(red: 0.9058823529, green: 0.2980392157, blue: 0.2352941176, alpha: 1) // E74C3C
    }
}

