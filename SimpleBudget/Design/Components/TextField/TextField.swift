//
//  TextField.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 18/03/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

@objc protocol TextFieldDelegate: class {
    @objc optional func textFieldDidTapped(_ textField: TextField)
}

@IBDesignable
class TextField: UIControl {

    
    // MARK: - Public Properties
    
    @IBInspectable
    public var placeholder: String? {
        set {
            self.textField.attributedPlaceholder = NSAttributedString(string: newValue!,
                                                                      attributes: [.foregroundColor : ColorPalette.Custom.BlueLighter])
        }
        get {
            return textField.placeholder
        }
    }
    
    @IBInspectable
    public var isEditable: Bool = true {
        didSet {
            textField.isEnabled = isEditable
        }
    }
    
    public var isSecureTextEntry: Bool {
        set {
            textField.isSecureTextEntry = newValue
        }
        get {
            return textField.isSecureTextEntry
        }
    }
    
    public var text: String? {
        set {
            textField.text = newValue
        }
        get {
            return textField.text
        }
    }
    
    
    // MARK: - UI Items
    
    let textField: UITextField = {
        let t = UITextField()
        t.textColor = ColorPalette.Custom.GrayLight
        t.tintColor = ColorPalette.Custom.Green
        return t
    }()
    
    let padding = UIEdgeInsets(top: 0,
                               left: 15,
                               bottom: 0,
                               right: 15)
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    // MARK: - Private Method
    
    private func commonInit() {
        // Text field
        addSubview(textField)
        
        configure()
    }
    
    private func configure() {
        // Container configuration
        set(cornerRadius: 8)
        backgroundColor = ColorPalette.Custom.BlueDark
        layer.borderColor = ColorPalette.Custom.Green.cgColor
        layer.borderWidth = 1
        
        // Shadow
//        let myLayer = CALayer()
//        myLayer.backgroundColor = UIColor.red.cgColor
//        myLayer.shadowOffset = CGSize(width: 0, height: 0)
//        myLayer.shadowColor = UIColor.black.cgColor
//        myLayer.shadowOpacity = 0.5
//        myLayer.shadowRadius = 5
//        myLayer.masksToBounds = false
//        myLayer.frame = bounds
//        layer.addSublayer(myLayer)
        
        configureTextFieldConstraints()
    }

    private func configureTextFieldConstraints() {
        // Add constraints for text field
        textField.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = textField.topAnchor.constraint(equalTo: self.topAnchor, constant: padding.top)
        let bottomConstraint = textField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -padding.bottom)
        let leftConstraint = textField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: padding.left)
        let rightConstraint = textField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -padding.right)
        let cons = [topConstraint, bottomConstraint, leftConstraint, rightConstraint]
        
        NSLayoutConstraint.activate(cons)
    }
}
