//
//  OverallExpenseIncomeSectionController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 26/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import IGListKit

class OverallExpenseIncomeSectionController: ListSectionController {

    fileprivate var model: OveralExpenseIncomeViewModel!
    fileprivate var expandedChartItem: OverallChartPart?
    
    override init() {
        super.init()
        inset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        minimumLineSpacing = 16
        scrollDelegate = self
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func didUpdate(to object: Any) {
        model = object as? OveralExpenseIncomeViewModel
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: OverallCollectionViewCell.self, for: self, at: index) as! OverallCollectionViewCell
        cell.expense = model.totalExpense
        cell.income = model.totalIncome
        cell.delegate = self
        setCategoryDetailChartCell(cell)
        return cell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else { return .zero }
        
        let width = context.containerSize.width
            - DefaultValues.CollectionView.LeftPadding
            - DefaultValues.CollectionView.RightPadding
        
        var height: CGFloat = 185

        if expandedChartItem != nil {
            height += 250
        }
        
        return CGSize(width: width, height: height)
    }
}

extension OverallExpenseIncomeSectionController: OverallChartDelegate {
    
    func chartDidHighlighted(part: OverallChartPart) {
        if expandedChartItem != part {
            expandedChartItem = part
        } else {
            expandedChartItem = nil
        }
        
        guard let cell = collectionContext?.visibleCells(for: self).last as? OverallCollectionViewCell else { return }
        setCategoryDetailChartCell(cell)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.6,
                       options: [],
                       animations: {
                        self.collectionContext?.invalidateLayout(for: self)
                        cell.setNeedsLayout()
        })
    }
    
    func setCategoryDetailChartCell(_ cell: OverallCollectionViewCell) {
        guard let selectedChartItem = expandedChartItem else {
            cell.categoryDetailChart.isHidden = true
            return
        }
        
        // TODO: Highlight selected item in Chart
        switch selectedChartItem {
        case .expense:
            cell.categoryDetailChart.title = "Expense"
            cell.categoryDetailChart.sumCategories = model.sumCategoriesExpense
        case .income:
            cell.categoryDetailChart.title = "Income"
            cell.categoryDetailChart.sumCategories = model.sumCategoriesIncome
        }
        cell.categoryDetailChart.isHidden = false
        
        
    }
}


extension OverallExpenseIncomeSectionController: ListScrollDelegate {
    
    func listAdapter(_ listAdapter: ListAdapter, didScroll sectionController: ListSectionController) {
        guard let collectionView = listAdapter.collectionView else { return }
        guard let scrollDelegate = self.viewController as? ScrollDelegate else { return }
        scrollDelegate.didScroll(offset: CGPoint(x: collectionView.contentOffset.x,
                                                 y: CGFloat(collectionView.contentOffset.y + collectionView.contentInset.top)))
    }
    
    func listAdapter(_ listAdapter: ListAdapter, willBeginDragging sectionController: ListSectionController) {
        guard let scrollDelegate = self.viewController as? ScrollDelegate else { return }
        scrollDelegate.willBeginDragging()
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDragging sectionController: ListSectionController, willDecelerate decelerate: Bool) {
        guard let scrollDelegate = self.viewController as? ScrollDelegate else { return }
        scrollDelegate.didEndDragging()
    }
}

