//
//  CategorySectionController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import IGListKit

protocol CategorySelectionDelegate: class {
    func categoryDidSelected (_ categoryViewModel: CategoryModel)
}

class CategorySectionController: ListSectionController {
    
    var categoryViewModel: CategoryViewModel?
    var isSelected = false
    
    override init() {
        super.init()
        
        let containerSize = UIScreen.main.bounds
        let remaining = containerSize.width - (DefaultValues.CategoryCollectionView.ItemWidth * 3)
        let padding: CGFloat = floor(remaining / 4)
        self.inset = UIEdgeInsets(top: padding, left: padding, bottom: 0, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func didUpdate(to object: Any) {
        categoryViewModel = object as? CategoryViewModel
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "CategoryCell",
                                                                bundle: nil,
                                                                for: self,
                                                                at: index) as? CategoryCell else {
                                                                    fatalError()
        }
        
        cell.labelName.text = categoryViewModel?.categoryModel.name
        cell.labelIcon.text = categoryViewModel?.categoryModel.icon
        
        return cell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = DefaultValues.CategoryCollectionView.ItemWidth
        return CGSize(width: width, height: width)
    }
    
    override func didSelectItem(at index: Int) {
        guard let viewController = viewController, let category = categoryViewModel else { return }
        (viewController as? CategorySelectionDelegate)?.categoryDidSelected(category.categoryModel)
        collectionContext?.cellForItem(at: index, sectionController: self)?.isSelected = true
    }
}
