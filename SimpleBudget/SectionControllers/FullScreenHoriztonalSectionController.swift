//
//  FullScreenHoriztonalSectionController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 28/03/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import IGListKit

protocol FullScreenHorizontalSectionDelegate {
    func didScrolledTo(index : Int)
}

class FullScreenHoriztonalSectionController: ListSectionController {
    
    fileprivate var feedItem: FeedItem!
    
    override init() {
        super.init()
        scrollDelegate = self
    }
    
    fileprivate lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(),
                                  viewController: self.viewController)
        adapter.dataSource = self
        return adapter
    }()
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func didUpdate(to object: Any) {
        feedItem = object as? FeedItem
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: FullScreenCell.self, for: self, at: index) as! FullScreenCell
        adapter.collectionView = cell.collectionView
        return cell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else { return .zero }
        
        return CGSize(width: context.containerSize.width,
                      height: context.containerSize.height -
                        context.containerInset.bottom -
                        context.containerInset.top)
    }
    
}


// MARK: - ListAdapterDataSource

extension FullScreenHoriztonalSectionController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return feedItem.items
    }
    
    public func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        
        guard object is MainCellCreator else {
            fatalError("Object \(object) doesn't implement MainCellCreator protocol")
        }
        
        return (object as! MainCellCreator).sectionController
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let v = UILabel()
        // TODO: Localize string:
        v.text = "No records found "
        v.textColor = ColorPalette.FlatUI.Asbestos
        v.textAlignment = .center
        v.alpha = 0
        UIView.animate(withDuration: 1) { v.alpha = 1 }
        return v
    }
    
}

extension FullScreenHoriztonalSectionController: ListScrollDelegate {
    func listAdapter(_ listAdapter: ListAdapter, didScroll sectionController: ListSectionController) {
        
    }
    
    func listAdapter(_ listAdapter: ListAdapter, willBeginDragging sectionController: ListSectionController) {
//        print("🤖 Will Begin Dragging")
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDragging sectionController: ListSectionController, willDecelerate decelerate: Bool) {
//        print("✅ Did End Dragging")
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDeceleratingSectionController sectionController: ListSectionController) {
        
        if let indexPaths = self.collectionContext?.visibleIndexPaths(for: sectionController),
            let viewController = self.viewController,
            indexPaths.count > 0
        {   
            (viewController as? FullScreenHorizontalSectionDelegate)?.didScrolledTo(index: indexPaths[0].section)
        }
        
    }
    
}
