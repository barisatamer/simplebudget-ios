//
//  MainSectionController.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 02/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import IGListKit

protocol ScrollDelegate: class {
    func didScroll(offset: CGPoint)
    func willBeginDragging()
    func didEndDragging()
}

class MainSectionController: ListSectionController {

    private var transaction: TransactionHistoryViewModel!
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func didUpdate(to object: Any) {
        transaction = object as? TransactionHistoryViewModel
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "TransactionHistoryCell",
                                                                bundle: nil,
                                                                for: self,
                                                                at: index) as? TransactionHistoryCell
            else {
                fatalError()
        }
        
        cell.labelName.text = transaction.transactionModel.name
        cell.amount = transaction.transactionModel.amount
        
        return cell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 66)
    }
    
    override func didSelectItem(at index: Int) {
        guard let viewController = self.viewController else { return }
        (viewController as? CellTapper)?.didTappedCell(withTransaction: transaction.transactionModel)
    }
    
}
