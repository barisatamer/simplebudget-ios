//
//  Date+Extension.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 08/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation

extension Date {

    static func currentMonth() -> Date {
        let cal: Calendar = Calendar.current
        var dateComponents = cal.dateComponents([Calendar.Component.year, Calendar.Component.month, Calendar.Component.day ], from: Date())
        dateComponents.setValue(1, for: Calendar.Component.day)
        dateComponents.setValue(1, for: Calendar.Component.hour)
        let currentMonth = cal.date(from: dateComponents)
        return currentMonth!
    }
    
    func monthName() -> String? {
        let dateFormatter = DateFormatter()
        let cal = Calendar.current
        var components = cal.dateComponents([Calendar.Component.year, Calendar.Component.month, Calendar.Component.day ], from: self)
        if let month = components.month {
            let monthName = dateFormatter.monthSymbols[month-1]
            return monthName
        }
        return nil
    }
    
    // TODO: Refactor the method name to startOfTheCurrentMonth
    func startOfMonth() -> Date? {
        let cal = Calendar.current
        var components = cal.dateComponents([Calendar.Component.year, Calendar.Component.month, Calendar.Component.day ], from: self)
        components.setValue(1, for: Calendar.Component.day)
        components.setValue(1, for: Calendar.Component.hour)
        return cal.date(from: components)
    }
 
    /// Returns the start of the month
    ///
    /// - Returns: Start of month
    func startOfTheMoth() -> Date {
        let components = Calendar.current.dateComponents([.year, .month], from: self)
        return Calendar.current.date(from: components)!
    }

    
    /// Returns the start of the year
    ///
    /// - Returns: Date, start of the year
    func startOfTheYear() -> Date {
        let components = Calendar.current.dateComponents([.year], from: self)
        return Calendar.current.date(from: components)!
    }
    
    
    /// Get the months of current year, starting from January
    /// January, 2018 - February 2018 . . .
    ///
    /// - Returns: Date array
    func monthsOfYear() -> [Date] {
        let startOfThisYear = self.startOfTheYear()
        var dates: [Date] = [startOfThisYear]
        
        for i in 1...11 {
            dates.append(startOfThisYear.nextMonth(count: i))
        }
        return dates
    }
    
    /// Returns a `Date` by going back {count} months later
    /// Default is one month
    ///
    /// - Parameter count: How many months later?
    /// - Returns: Date {count} month ago
    func nextMonth(count: Int = 1) -> Date {
        return Calendar.current.date(byAdding: Calendar.Component.month, value: count * 1, to: self)!
    }
    
    func previousYear() -> Date {
        return Calendar.current.date(byAdding: Calendar.Component.year, value: -1, to: self)!
    }
    
    
    func withoutTime() -> Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    /// Returns string representation with month and year
    ///
    /// - Returns: String like `March 2018`
    func stringWithMonthYear() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        return formatter.string(from: self)
    }

    /// Returns string representation with year
    ///
    /// - Returns: String, ex: `2018`
    func stringWithYear() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        return formatter.string(from: self)
    }
    
    func debugString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: self)
    }

    /// Returns a `Date` by going back {count} months ago
    /// Default is one month
    ///
    /// - Parameter count: How many months ago?
    /// - Returns: Date {count} month ago
    func pastMonth(count: Int = 1) -> Date {
        return Calendar.current.date(byAdding: Calendar.Component.month, value: count * -1, to: self)!
    }

}
