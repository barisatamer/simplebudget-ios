//
//  UIVewController+Extension.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 01/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    public static var defaultNib: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    public static var storyboardIdentifier: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    public func removeBackButtonTitle() {
        let barButton = UIBarButtonItem()
        barButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = barButton
    }
    
    public func embedInNavigationBar() -> UINavigationController {
        let navC = UINavigationController(rootViewController: self)
        navC.navigationBar.barTintColor = ColorPalette.Custom.Green
        navC.navigationBar.tintColor = .white
        navC.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        return navC
    }
    
    public func presentByEmbeddingInNavigationBar(animated: Bool, from parentViewController: UIViewController) {
        parentViewController.present(embedInNavigationBar(), animated: true, completion: nil)
    }
}

