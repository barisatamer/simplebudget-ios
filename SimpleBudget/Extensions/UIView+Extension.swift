//
//  UIView+Extension.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 18/03/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

public extension UIView {
    func set(cornerRadius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
    }
    
    func addShadowToCard() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.2
        self.layer.shadowColor = UIColor.black.cgColor
    }
    
    func addBorderToCard() {
        self.layer.borderColor = ColorPalette.Custom.BlueLighter.cgColor
        self.layer.borderWidth = 1
    }
    
    /// Adds a border line on the bottom edge
    /// - Parameters:
    ///   - color: Color of the border
    ///   - width: Width of the border
    func addBottomBorder(_ color: UIColor, thickness: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(border)
        
        border.translatesAutoresizingMaskIntoConstraints = false
        border.heightAnchor.constraint(equalToConstant: thickness).isActive = true
        border.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        border.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        border.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    

}
