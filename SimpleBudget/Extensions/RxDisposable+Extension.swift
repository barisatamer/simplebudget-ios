//
//  RxDisposable+Extension.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import RxSwift

protocol RxDisposable {
    var disposeBag: DisposeBag { get set }
}

extension ObservableType {
    
    func subscribe(_ controller: RxDisposable,
                   onNext: ((E) -> Void)? = nil,
                   onError: ((Swift.Error) -> Void)? = nil,
                   onCompleted: (() -> Void)? = nil,
                   onDisposed: (() -> Void)? = nil) {
        return self.subscribe(onNext: onNext,
                              onError: onError,
                              onCompleted: onCompleted,
                              onDisposed: onDisposed)
            .addDisposableTo(controller.disposeBag)
    }
    
}
