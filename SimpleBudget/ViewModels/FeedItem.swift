//
//  FeedItem.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 23/03/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import IGListKit

typealias CategorySum = ([CategoryChartItem], [CategoryChartItem])

class FeedItem: ListDiffable {
    let uuid = UUID()
    let headerTitle: String
    let date: Date
    var items = [ListDiffable]()
    
    
    init(headerTitle: String, date: Date, items: [ListDiffable]) {
        self.headerTitle = headerTitle
        self.date = date
        self.items.append(contentsOf: items)
        prepareOverallViewModel(items: items)
    }
    
    func prepareOverallViewModel(items: [ListDiffable]) {
        // Calculate total income and expense
        var totalExpenses: Double = 0
        var totalIncomes: Double = 0
        items.forEach {
            guard let transactionViewModel = $0 as? TransactionHistoryViewModel else { return }
            
            if transactionViewModel.transactionModel.amount < 0 {
                totalExpenses += transactionViewModel.transactionModel.amount
            } else {
                totalIncomes += transactionViewModel.transactionModel.amount
            }
        }
        
        let sumCat = sumCategories()
        
        // Create the view model
        let overall = OveralExpenseIncomeViewModel(totalIncome: totalIncomes,
                                                   totalExpense: totalExpenses,
                                                   sumCategoriesIncome: sumCat.0,
                                                   sumCategoriesExpense: sumCat.1)
        self.items.insert(overall, at: 0)
    }
    
    func sumCategories() -> CategorySum {
        var expenses = [String: CategoryChartItem]()
        var incomes = [String: CategoryChartItem]()
        
        items.forEach {
            if let transactionViewModel = $0 as? TransactionHistoryViewModel {
                
                let model = transactionViewModel.transactionModel
                
                if model.amount < 0 { // Expense
                    if var item = expenses[model.categoryName] {
                        item.totalAmount += model.amount
                    } else {
                        expenses[model.categoryName] = CategoryChartItem(categoryName: model.categoryName, totalAmount: model.amount, icon: nil)
                    }
                } else { // Income
                    if var item = incomes[model.categoryName] {
                        item.totalAmount += model.amount
                    } else {
                        incomes[model.categoryName] = CategoryChartItem(categoryName: model.categoryName, totalAmount: model.amount, icon: nil)
                    }
                }
            }
        }
        
        return (Array(incomes.values), Array(expenses.values))
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return uuid.uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? FeedItem else { return false }
        return object.uuid == uuid
    }
}
