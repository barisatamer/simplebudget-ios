//
//  MainCellCreator.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 13/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import IGListKit

public protocol MainCellCreator {
    var sectionController: ListSectionController { get }
}
