//
//  OveralExpenseIncomeViewModel.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 26/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import IGListKit

class OveralExpenseIncomeViewModel: ListDiffable, MainCellCreator {
    
    let uuid = UUID()
    let totalIncome: Double
    let totalExpense: Double
    let sumCategoriesIncome: [CategoryChartItem]
    let sumCategoriesExpense: [CategoryChartItem]
    
    init(totalIncome: Double, totalExpense: Double, sumCategoriesIncome: [CategoryChartItem], sumCategoriesExpense: [CategoryChartItem]) {
        self.totalIncome = totalIncome
        self.totalExpense = totalExpense
        self.sumCategoriesIncome = sumCategoriesIncome
        self.sumCategoriesExpense = sumCategoriesExpense
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return uuid.uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? OveralExpenseIncomeViewModel else { return false }
        return object.uuid == uuid
    }
    
    public var sectionController: ListSectionController {
        return OverallExpenseIncomeSectionController()
    }
}
