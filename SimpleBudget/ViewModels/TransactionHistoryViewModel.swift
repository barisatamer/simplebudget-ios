//
//  TransactionHistoryViewModel.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 02/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import IGListKit


class TransactionHistoryViewModel: ListDiffable, MainCellCreator {
    
    let transactionModel: TransactionModel
    
    init(transactionModel: TransactionModel) {
        self.transactionModel = transactionModel
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return "\(transactionModel.id!)" as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? TransactionHistoryViewModel else { return false }
        return
            object.transactionModel.name == self.transactionModel.name
            && object.transactionModel.desc == self.transactionModel.desc
            && object.transactionModel.amount == self.transactionModel.amount
    }
    
    var sectionController: ListSectionController {
        return MainSectionController()
    }
}
