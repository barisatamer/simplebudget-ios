//
//  CategoryViewModel.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import IGListKit

class CategoryViewModel: ListDiffable {
    
    let categoryModel: CategoryModel
    
    init(categoryModel: CategoryModel) {
        self.categoryModel = categoryModel
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return "\(categoryModel.id!)" as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? CategoryViewModel else { return false }
        return object.categoryModel.id == self.categoryModel.id
    }
}
