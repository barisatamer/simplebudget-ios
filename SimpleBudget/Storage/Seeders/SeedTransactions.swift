//
//  SeedTransactions.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 08/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import GRDB

class SeedTransactions {
    
    // remove these commented out lines
//    private static let categories = [
//        CategoryModel(name: "Food", icon: "🍉"),
//        CategoryModel(name: "Drink", icon: "🍷"),
//        CategoryModel(name: "Rent", icon: "🏠"),
//        CategoryModel(name: "Transportation", icon: "🚎"),
//        CategoryModel(name: "Travel", icon: "🚗"),
//        CategoryModel(name: "Sport", icon: "🏋🏻"),
//        CategoryModel(name: "Other", icon: "📝"),
//        CategoryModel(name: "Education", icon: "📝"),
//        ]
    
    private static let transactions = [
        TransactionModel(name: "Beer",
                         description: "",
                         currency: "eur",
                         amount: -5,
                         categoryId: 2),
        
        TransactionModel(name: "Wine",
                         description: "",
                         currency: "eur",
                         amount: -9,
                         categoryId: 2),
        
        TransactionModel(name: "Rent",
                         description: "",
                         currency: "eur",
                         amount: -460,
                         categoryId: 3),
        
        TransactionModel(name: "Gas",
                         description: "",
                         currency: "eur",
                         amount: -10,
                         categoryId: 4),
        
        TransactionModel(name: "Gas",
                         description: "",
                         currency: "eur",
                         amount: -15,
                         categoryId: 4),
        
        TransactionModel(name: "Train ticket",
                         description: "",
                         currency: "eur",
                         amount: -80,
                         categoryId: 4),
        
        TransactionModel(name: "Gym membership",
                         description: "",
                         currency: "eur",
                         amount: -90,
                         categoryId: 6),
        
        TransactionModel(name: "Udemy Machine Learning Course",
                         description: "",
                         currency: "eur",
                         amount: -120,
                         categoryId: 8),
    ]
    
    class func seed (_ db: Database) throws {
        
        // Current month
        try transactions.forEach {
            try $0.insert(db)
        }
        try seedRandomExpense(db: db, date: Date())
        
        // Past month
        try transactions.forEach {
            $0.createdAt = Date().pastMonth()
            try $0.insert(db)
        }
        try seedRandomExpense(db: db, date: Date().pastMonth())
        
        // 2 month ago
        try transactions.forEach {
            $0.createdAt = Date().pastMonth(count: 2)
            try $0.insert(db)
        }
        try seedRandomExpense(db: db, date: Date().pastMonth(count: 2))
        
        // 3 month ago
        try transactions.forEach {
            $0.createdAt = Date().pastMonth(count: 3)
            try $0.insert(db)
        }
        try seedRandomExpense(db: db, date: Date().pastMonth(count: 3))
        
        // 4 month ago
        try transactions.forEach {
            $0.createdAt = Date().pastMonth(count: 4)
            try $0.insert(db)
        }
        try seedRandomExpense(db: db, date: Date().pastMonth(count: 4))
    }
    
    class func seedRandomExpense(db: Database, date: Date) throws {
        for _ in 0...30 {
            let transaction = TransactionModel(name: randomName(),
                                               description: "",
                                               currency: "eur",
                                               amount: Double(Int.random(range: -20 ... -5)),
                                               categoryId: 2)
            transaction.createdAt = date
            try transaction.insert(db)
        }
    }
    
    class func randomName() -> String {
            return expenseNames[Int(arc4random_uniform(UInt32(expenseNames.count)))]
        }
  
    static let expenseNames = ["Beer", "Beverage", "Dinner", "Hamburger", "Wine", "Bike rent",
        "Launch", "Breakfast", "Bus ticket", "Train ticket"
    ]
}


extension Int
{
    static func random(range: CountableClosedRange<Int> ) -> Int
    {
        var offset = 0
        
        if range.lowerBound < 0
        {
            offset = abs(range.lowerBound)
        }
        
        let mini = UInt32(range.lowerBound + offset)
        let maxi = UInt32(range.upperBound   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
}

