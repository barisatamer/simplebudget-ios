//
//  SeedCategories.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 08/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import GRDB

class SeedCategories {
    
    // TODO: Add localization for Category names
    private static let categories = [
        CategoryModel(name: "Food", icon: "🍉"),
        CategoryModel(name: "Drink", icon: "🍷"),
        CategoryModel(name: "Rent", icon: "🏠"),
        CategoryModel(name: "Transportation", icon: "🚎"),
        CategoryModel(name: "Travel", icon: "🚗"),
        CategoryModel(name: "Sport", icon: "🏋🏻"),
        CategoryModel(name: "Education", icon: "📚"),
        CategoryModel(name: "Other", icon: "📝"),
        ]
    
    class func seed (_ db: Database) throws {
        try categories.forEach { try $0.insert(db) }
    }
}
