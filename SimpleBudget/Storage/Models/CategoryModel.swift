//
//  CategoryModel.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import GRDB

class CategoryModel: Record {
    
    var id: Int64?
    var name: String
    var icon: String?
    
    override class var databaseTableName: String {
        return "Categorys"
    }
    
    enum Columns {
        static let id = Column("id")
        static let name = Column("name")
        static let icon = Column("icon")
    }
    
    init(name: String, icon: String?, id: Int64? = nil) {
        self.name = name
        self.icon = icon
        self.id = id
        super.init()
    }
    
    required init(row: Row) {
        id = row[Columns.id]
        name = row[Columns.name]
        icon = row[Columns.icon]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[Columns.id] = id
        container[Columns.name] = name
        container[Columns.icon] = icon
    }
    
}
