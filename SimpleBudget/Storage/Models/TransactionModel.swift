//
//  TransactionModel.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import GRDB

class TransactionModel: Record {
    
    var id: Int64?
    var name: String
    var desc: String
    var currency: String
    var amount: Double
    var categoryId: Int64 = 0
    var createdAt: Date?
    var categoryName: String
    
    override class var databaseTableName: String {
        return "transactions"
    }
    
    enum Columns {
        static let id = Column("id")
        static let name = Column("name")
        static let description = Column("description")
        static let currency = Column("currency")
        static let amount = Column("amount")
        static let categoryId = Column("category_id")
        static let createdAt = Column("created_at")
    }
    
    init(name: String, description: String, currency: String, amount: Double, categoryId: Int64, createdAt: Date? = nil) {
        self.name = name
        self.desc = description
        self.currency = currency
        self.amount = amount
        self.categoryId = categoryId
        self.createdAt = createdAt
        if self.createdAt == nil {
            self.createdAt = Date().withoutTime()
        }
        categoryName = ""
        super.init()
    }
    
    required init(row: Row) {
        id = row[Columns.id]
        name = row[Columns.name]
        desc = row[Columns.description]
        currency = row[Columns.currency]
        amount = row[Columns.amount]
        categoryId = row[Columns.categoryId]
        createdAt = row[Columns.createdAt]
        // Date.fromDatabaseValue(dbValue)!
        categoryName = row["categoryName"]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[Columns.id] = id
        container[Columns.name] = name
        container[Columns.description] = desc
        container[Columns.currency] = currency
        container[Columns.amount] = amount
        container[Columns.categoryId] = categoryId
        container[Columns.createdAt] = createdAt
    }
    
}
