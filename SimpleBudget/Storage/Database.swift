//
//  Database.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import GRDB
import UIKit

var database: DatabasePool!

// Add database migrations here
fileprivate let migrations: [DatabaseMigration] = [
    M1_CreateCategoriesTable(),
    M2_CreateTransactionsTable(),
    M3_CreateTagsTable(),
    M4_CreateTagsTransactionsPivotTable(),
    
]

// https://github.com/groue/GRDB.swift/issues/164#issuecomment-312468700
fileprivate func databaseDir() -> String {
    let documentsPath = NSSearchPathForDirectoriesInDomains(
        .documentDirectory, .userDomainMask, true).first! as NSString
    
    return documentsPath.appendingPathComponent("db")
}

fileprivate func databasePath() throws -> String {
    let dbPath = databaseDir()
    try FileManager.default.createDirectory(atPath: dbPath, withIntermediateDirectories: true)
    return NSString(string: dbPath).appendingPathComponent("db.sqlite")
}

func setupDatabase(_ application: UIApplication) throws {
    
    log.debug("Initializing GRDB iOS database")
    
    // Register SQLite error logger
    Database.logError = { (resultCode, message) in
        log.error("SQLite error \(resultCode): \(message)")
    }
    
    // Connect to the database
    // See https://github.com/groue/GRDB.swift/#database-connections
    let dbPath = try databasePath()
    log.debug("Database path is: \(dbPath)")
    database = try DatabasePool(path: dbPath)
    
    // See https://github.com/groue/GRDB.swift/#memory-management
    database.setupMemoryManagement(in: application)
    
    // Register all migrations and perform them
    var migrator = DatabaseMigrator()
    
    migrations.forEach { migrator.add($0) }
    
    try migrator.migrate(database)
    
    // Seed
    try Category.seedCategories()
}

func dropDatabase(_ application: UIApplication) {
    database = nil
    
    do {
        try FileManager.default.removeItem(atPath: databaseDir())
    } catch {
        log.info("Could not drop database. \(error)")
    }
}

func dropAndRecreateDatabase(_ application: UIApplication) throws {
    log.debug("Drop database")
    
    dropDatabase(application)
    try setupDatabase(application)
    
    log.debug("Database dropped and successfully recreated")
}
