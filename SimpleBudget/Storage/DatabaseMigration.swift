import Foundation
import GRDB

protocol DatabaseMigration {
    func migrationIdentifier() -> String
    func migrate(_ db: Database) throws -> Void
}

extension DatabaseMigrator {
    mutating func add(_ migration: DatabaseMigration) {
        self.registerMigration(migration.migrationIdentifier()) { db in
            log.debug("Performing database migration: \(migration.migrationIdentifier())")
            try migration.migrate(db)
             log.debug("Database migration: \(migration.migrationIdentifier()) done")
        }
    }
}
