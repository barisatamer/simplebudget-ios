//
//  M3_CreateTagsTable.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

/*
  __________________
 |       Tags       |
 |==================|
 |   id             |
 |   name           |
 |__________________|
 
 */

import Foundation
import GRDB

class M3_CreateTagsTable: DatabaseMigration {
    
    func migrationIdentifier() -> String {
        return "Create tags table"
    }
    
    func migrate(_ db: Database) throws {
        
        try db.create(table: "tags") { t in
            // An integer primary key auto-generates unique IDs
            t.column("id", .integer).primaryKey()
            t.column("name", .text)
                .notNull()
                .collate(.localizedCaseInsensitiveCompare)
        }
    }
    
}
