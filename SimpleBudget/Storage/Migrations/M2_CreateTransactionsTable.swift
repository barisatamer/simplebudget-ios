//
//  M2_CreateTransactionsTable.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

/*
  __________________
 |   Transactions   |
 |==================|
 |   id             |
 |   name           |
 |   description    |
 |   currency       |
 |   amount         |
 |   category_id *  |
 |   created_at     |
 |__________________|
 
 */

import Foundation
import GRDB

class M2_CreateTransactionsTable: DatabaseMigration {
    
    func migrationIdentifier() -> String {
        return "Create transactions table"
    }
    
    func migrate(_ db: Database) throws {
        
        try db.create(table: "transactions") { t in
            // An integer primary key auto-generates unique IDs
            t.column("id", .integer).primaryKey()
            t.column("name", .text).notNull().collate(.localizedCaseInsensitiveCompare)
            t.column("description", .text).notNull().collate(.localizedCaseInsensitiveCompare)
            t.column("currency", .text)
            t.column("amount", .double).notNull()
            t.column("category_id", .integer).references("categorys",
                                                         column: "id",
                                                         onDelete: .cascade,
                                                         onUpdate: .cascade,
                                                         deferred: false)
            t.column("created_at", .date)
        }
    }
    
}
