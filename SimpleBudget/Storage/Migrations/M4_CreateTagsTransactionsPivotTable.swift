//
//  M4_CreateTagsTransactionsPivotTable.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//
/*
  ___________________
 | transaction_tags  |
 |===================|
 |   id              |
 |   transaction_id *|
 |   tag_id  *       |
 |                   |
 |___________________|
 */

import Foundation
import GRDB

class M4_CreateTagsTransactionsPivotTable: DatabaseMigration {
    
    func migrationIdentifier() -> String {
        return "Create transaction_tag table"
    }
    
    func migrate(_ db: Database) throws {
        try db.execute("""
            CREATE TABLE transaction_tag (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                transaction_id INTEGER NOT NULL,
                tag_id INTEGER NOT NULL,
                FOREIGN KEY (transaction_id) REFERENCES transactions(id) ON DELETE CASCADE,
                FOREIGN KEY (tag_id) REFERENCES tags(id) ON DELETE CASCADE,
                UNIQUE(transaction_id, tag_id)
            )
        """)
    }
    
}
