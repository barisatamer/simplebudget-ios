//
//  M1_CreateCategoriesTable.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//
/*
  ____________________
 |     Categorys      |
 |====================|
 |   id               |
 |   name             |
 |                    |
 |____________________|
 */

import Foundation
import GRDB

class M1_CreateCategoriesTable: DatabaseMigration {
    
    func migrationIdentifier() -> String {
        return "Create categories table"
    }
    
    func migrate(_ db: Database) throws {
        
        try db.create(table: "categorys") { t in
            // An integer primary key auto-generates unique IDs
            t.column("id", .integer).primaryKey() 
            t.column("name", .text).notNull().collate(.localizedCaseInsensitiveCompare)
            t.column("icon", .text)
        }
    }
    
}
