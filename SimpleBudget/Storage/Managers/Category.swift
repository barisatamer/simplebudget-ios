//
//  Category.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import GRDB
import RxSwift
import RxGRDB

class Category {
    
    public static func all() -> Observable<[CategoryViewModel]> {
        return SQLRequest(get_category_all, arguments: [])
            .asRequest(of: CategoryModel.self)
            .rx.fetchAll(in: database, distinctUntilChanged: true)
            .map { (messages: [CategoryModel]) -> [CategoryViewModel] in
                var out = [CategoryViewModel]()
                messages.forEach{
                    out.append(CategoryViewModel(categoryModel: $0))
                }
                return out
            }.observeOn(MainScheduler.instance)
    }

    public static func save(_ category: CategoryModel) throws {
        try database.writeInTransaction { db in
            try category.insert(db)
            return .commit
        }
    }
    
    public static func seedCategories() throws {
        guard let count = try categoryCount(), count == 0 else { return }
        
        try database.writeInTransaction { db in
            try SeedCategories.seed(db)
            try SeedTransactions.seed(db)
            return .commit
        }
    }
 
    public static func categoryCount() throws -> Int64? {
        return try database.read { db in
            try SQLRequest(get_category_count, arguments: [])
                .asRequest(of: Int64.self)
                .fetchOne(db)
        }
    }
    
    // MARK: - SQL Queries
    
    private static let get_category_count = "SELECT COUNT(*) FROM Categorys"
    private static let get_category_all = "SELECT * FROM Categorys"
    
}

