//
//  Transaction.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 04/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import GRDB
import RxSwift
import RxGRDB

class Transaction {
    
    public static func getTransactions(forYear year: Date) -> Observable<[FeedItem]> {
        return SQLRequest(get_transactions, arguments: [year])
            .asRequest(of: TransactionModel.self)
            .rx.fetchAll(in: database, distinctUntilChanged: true)
            .map { (messages: [TransactionModel]) -> [FeedItem] in
                
                let groupedByMonth = Dictionary(grouping: messages, by: {
                    $0.createdAt!.startOfTheMoth()
                })
                
                var monthsTransactionsDict: [Date: [TransactionModel]] = [:]
                //for month in Date().monthsOfYear() {
                for month in year.monthsOfYear() {
                
                    if let transactions = groupedByMonth[month] {
                        monthsTransactionsDict[month] = transactions
                    } else {
                        monthsTransactionsDict[month] = []
                    }
                }
                
                var out2 = [FeedItem]()
                for month in monthsTransactionsDict.keys.sorted() {
                    var transactions = [TransactionHistoryViewModel]()
                    
                    if let values = monthsTransactionsDict[month] {
                        values.forEach {
                            transactions.append(TransactionHistoryViewModel(transactionModel: $0))
                        }
                    }
                    
                    out2.append(FeedItem(headerTitle: month.stringWithMonthYear(), date: month, items: transactions))
                }
                
                return out2
            }.observeOn(MainScheduler.instance)
    }
    
    public static func save(transaction: TransactionModel) throws {
        try database.writeInTransaction { db in
            
            if transaction.id != nil {
                try transaction.update(db)
            } else {
                try transaction.insert(db)
            }
            
            return .commit
        }
    }
    
    private static let get_transactions = """
        SELECT transactions.*, categorys.name AS categoryName
        FROM transactions, categorys
        WHERE transactions.category_id = categorys.id
        AND created_at > ?
        ORDER BY transactions.created_at DESC
    """
}
