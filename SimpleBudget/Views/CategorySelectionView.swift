//
//  CategorySelectionView.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import IGListKit

class CategorySelectionView: UIView {

    
    // MARK: - Private properties
    
    private var contentview: UIView!
    
    private static var identifier: String {
        return String(describing: self)
    }
    
    
    // MARK: - Public properties
    
    let collectionView: UICollectionView = {
        let layout = ListCollectionViewLayout.init(stickyHeaders: false, scrollDirection: UICollectionViewScrollDirection.vertical, topContentInset: 60, stretchToEdge: false)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    
    // MARK: - Initializer
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    // MARK: - Private Methods
    
    private func commonInit() {
        contentview = Bundle.main.loadNibNamed(CategorySelectionView.identifier, owner: self, options: nil)?.first! as! UIView
        contentview.frame = bounds
        addSubview(contentview)
        
        addSubview(collectionView)
        configure()
    }
    
    private func configure() { }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = bounds
    }
}
