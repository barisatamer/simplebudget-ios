//
//  OverallCollectionViewCell.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 26/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import Charts

enum OverallChartPart: Int {
    case expense, income
}

protocol OverallChartDelegate: class {
    func chartDidHighlighted(part: OverallChartPart)
}

class OverallCollectionViewCell: UICollectionViewCell {
    
    
    // MARK: - IB Outlets
    
    @IBOutlet weak var labelIncomeTitle: UILabel!
    @IBOutlet weak var labelIncome: UILabel!
    @IBOutlet weak var labelExpenseTitle: UILabel!
    @IBOutlet weak var labelExpense: UILabel!
    @IBOutlet weak var pieChart: PieChartView!
    var categoryDetailChart: ChartCategoryDetailViewCell = {
        let v = ChartCategoryDetailViewCell()
        v.isHidden = true
        return v
    }()
    
    // MARK: - Private Properties
    
    private var view: UIView!
    
    
    // MARK: - Public Properties
    
    public var income: Double = 0 {
        didSet {
            labelIncome.text = "\(abs(Int(income)))€"
            configureChart()
        }
    }
    
    public var expense: Double = 0 {
        didSet {
            labelExpense.text = "\(abs(Int(expense)))€"
            configureChart()
        }
    }
    
    weak var delegate: OverallChartDelegate?
    
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    // MARK: - Private Methods
    
    private func commonInit() {
        view = Bundle.main.loadNibNamed("OverallCollectionViewCell", owner: self, options: nil)?.first! as! UIView
        view.frame = bounds
        contentView.addSubview(view)
        
        contentView.addSubview(categoryDetailChart)
        configure()
    }
    
    private func configure() {
        view.backgroundColor = ColorPalette.Custom.BlueLight
        view.addBorderToCard()
        view.addShadowToCard()
        
        // Label colors
        labelIncomeTitle.textColor = ColorPalette.Custom.Gray
        labelExpenseTitle.textColor = ColorPalette.Custom.Gray
        labelIncome.textColor = ColorPalette.Custom.GreenLighter
        labelExpense.textColor = ColorPalette.Custom.RedLight
        
        configureChart()
        addCategoryDetailChartConstraints()
    }
    
    private func addCategoryDetailChartConstraints() {
        categoryDetailChart.translatesAutoresizingMaskIntoConstraints = false
        categoryDetailChart.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        categoryDetailChart.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        categoryDetailChart.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        categoryDetailChart.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 200).isActive = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        categoryDetailChart.isHidden = true
    }
}

extension OverallCollectionViewCell: ChartViewDelegate {
    
    private func configureChart() {
        pieChart.delegate = self as ChartViewDelegate
        pieChart.legend.enabled = false
        pieChart.setExtraOffsets(left: 0, top: 0, right: -20, bottom: 0)
        
        pieChart.holeColor = ColorPalette.FlatUI.WetAsphalt
        pieChart.chartDescription?.text = ""
        
        pieChart.drawEntryLabelsEnabled = false
        
        pieChart.spin(duration: 150,
                          fromAngle: pieChart.rotationAngle,
                          toAngle: pieChart.rotationAngle + 720,
                          easingOption: .easeInOutCubic)
        setData()
    }
    
    private func setData() {
        
        let total = abs(expense) + abs(income)
        let incomePercentage = calculatePercentage(for: abs(expense), total: total)
        let expensePercentage = calculatePercentage(for: abs(income), total: total)
        
        // Data entries
        let expenses = PieChartDataEntry(value: expensePercentage, label: "Expenses")
        let incomes = PieChartDataEntry(value: incomePercentage, label: "Incomes")
        
        let set = PieChartDataSet(values: [incomes, expenses], label: "Overall")
        set.sliceSpace = 2
        set.drawValuesEnabled = false
        set.colors = [
                      ColorPalette.Custom.RedLight,
                      ColorPalette.Custom.GreenLighter,
        ]
        
        set.valueLinePart1OffsetPercentage = 0.8
        set.valueLinePart1Length = 0.2
        set.valueLinePart2Length = 0.4
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        data.setValueFont(.systemFont(ofSize: 9, weight: .bold))
        data.setValueTextColor(ColorPalette.Custom.Gray)
        
        pieChart.data = data
        pieChart.highlightValues(nil)
    }
    
    private func calculatePercentage(for value: Double, total: Double) -> Double {
        return (value / total) * 100
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let part = OverallChartPart.init(rawValue: Int(highlight.x)) {
            delegate?.chartDidHighlighted(part: part)
        }
    }
}
