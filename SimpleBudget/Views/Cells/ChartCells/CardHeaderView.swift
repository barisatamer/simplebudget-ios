//
//  CardHeaderView.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 26/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

@IBDesignable
class CardHeaderView: UIView {

    @IBInspectable var text: String? {
        didSet {
            titleLabel.text = text
        }
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = ColorPalette.Custom.Gray
        return label
    }()
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - Private Method
    
    private func commonInit() {
        addSubview(titleLabel)
        configure()
    }
    
    private func configure() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        backgroundColor = .clear
        
        addBottomBorder(ColorPalette.Custom.BlueLighter, thickness: 1)
    }
}
