//
//  LabelWithBox.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 27/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

class LabelWithBox: UIView {

    
    // MARK: - Public Properties
    
    var color: UIColor? {
        set {
            boxView.backgroundColor = newValue
        }
        get {
            return boxView.backgroundColor
        }
    }
    
    var text: String? {
        set {
            label.text = newValue
        }
        get {
            return label.text
        }
    }
    
    let boxView: UIView = {
        let v = UIView()
        v.backgroundColor = .yellow
        return v
    }()
    
    let label: UILabel = {
        let l = UILabel()
        l.textColor = ColorPalette.Custom.GrayLight
        l.font = UIFont.systemFont(ofSize: 12)
        return l
    }()
    
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    convenience init(color: UIColor, text: String) {
        self.init()
        self.color = color
        self.text = text
    }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        addSubview(boxView)
        addSubview(label)
        
        configure()
    }
    
    private func configure() {
        addBoxConstraints()
        addLabelConstraints()
    }

    private func addBoxConstraints() {
        boxView.translatesAutoresizingMaskIntoConstraints = false
        boxView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        boxView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        boxView.widthAnchor.constraint(equalToConstant: 12).isActive = true
        boxView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    private func addLabelConstraints() {
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: topAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: boxView.rightAnchor, constant: 8).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 1.0, height: 1.0)
    }
}
