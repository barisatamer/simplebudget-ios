//
//  ChartCategoryDetailViewCell.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 27/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import Charts

class ChartCategoryDetailViewCell: UIView {
    
    // MARK: - IB Outlets
    
    @IBOutlet weak var titleHeader: CardHeaderView!
    @IBOutlet weak var stackViewCategories: UIStackView!
    @IBOutlet weak var pieChart: PieChartView!
    
    let FlatColors = [
        ColorPalette.FlatUI.Amethyst,
        ColorPalette.FlatUI.Asbestos,
        ColorPalette.FlatUI.BelizeHole,
        ColorPalette.FlatUI.Carrot,
        ColorPalette.FlatUI.Clouds,
        ColorPalette.FlatUI.Concrete,
        ColorPalette.FlatUI.MidnightBlue,
        ColorPalette.FlatUI.Orange,
        ColorPalette.FlatUI.PeterRiver,
        ColorPalette.FlatUI.Silver,
        ColorPalette.FlatUI.SunFlower,
        ColorPalette.FlatUI.Turquoise,
        ColorPalette.FlatUI.WetAsphalt,
        ColorPalette.FlatUI.Wisteria
    ]
    
    // MARK: - Private Properties
    
    private var view: UIView!
    
    
    // MARK: - Public Properties
    
    var title: String? {
        set {
            titleHeader.text = newValue
        }
        get {
            return titleHeader.text
        }
    }
    
    var total = 0.0
    var sumCategories: [CategoryChartItem] = [] {
        didSet {
            total = sumCategories.reduce(0) { (result, item) -> Double in
                return result + item.totalAmount
            }
            configureChart()
        }
    }
    
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    // MARK: - Private Methods
    
    private func commonInit() {
        view = Bundle.main.loadNibNamed("ChartCategoryDetailViewCell", owner: self, options: nil)?.first! as! UIView
        view.frame = bounds
        addSubview(view)
        configure()
    }
    
    private func configure() {
        view.backgroundColor = .clear
    }
}

extension ChartCategoryDetailViewCell {
    
    private func configureChart() {
        pieChart.legend.enabled = false
        pieChart.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        pieChart.layer.masksToBounds = false
        pieChart.clipsToBounds = false
        
        pieChart.holeColor = ColorPalette.FlatUI.WetAsphalt
        pieChart.chartDescription?.text = ""
        
        pieChart.drawEntryLabelsEnabled = true
        
        pieChart.spin(duration: 100,
                      fromAngle: pieChart.rotationAngle,
                      toAngle: pieChart.rotationAngle + 720,
                      easingOption: .linear)
        setData()
    }
    
    private func setData() {
        let entries = sumCategories.map { (item) -> PieChartDataEntry in
            return PieChartDataEntry(value: (item.totalAmount / total) * 100, label: item.categoryName)
        }
        
        let set = PieChartDataSet(values: entries, label: "")
        set.sliceSpace = 2
        set.drawValuesEnabled = true
        set.colors = FlatColors
        
        set.valueLinePart1OffsetPercentage = 0.8
        set.valueLinePart1Length = 0.2
        set.valueLinePart2Length = 0.4
         set.xValuePosition = .outsideSlice
         set.yValuePosition = .outsideSlice
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        data.setValueFont(.systemFont(ofSize: 9, weight: .bold))
        data.setValueTextColor(ColorPalette.Custom.Gray)
        
        pieChart.data = data
        pieChart.highlightValues(nil)
    }
}
