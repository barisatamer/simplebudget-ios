//
//  CategoryCell.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 05/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var labelIcon: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 84/255.0, green: 106/255.0, blue: 128/255.0, alpha: 1)
        self.selectedBackgroundView = view
        
        set(cornerRadius: 8)
        backgroundColor = ColorPalette.Custom.BlueDark
        layer.borderColor = ColorPalette.Custom.Green.cgColor
        layer.borderWidth = 1
        
        /*
        var transform:CATransform3D = CATransform3DMakeRotation(0.08, 0, 0, 1.0)
        var animation:CABasicAnimation = CABasicAnimation(keyPath: "transform")
        
        animation.toValue = NSValue(caTransform3D: transform)
        animation.autoreverses = true
        animation.duration = 0.5
        animation.repeatCount = 10000
        layer.add(animation, forKey: "wiggleAnimation")*/
        
        // startWiggling()
    }
    
    func startWiggling() {
        guard layer.animation(forKey: "wiggle") == nil else { return }
        guard layer.animation(forKey: "bounce") == nil else { return }
        
        let angle = 0.04
        
        let wiggle = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        wiggle.values = [-angle, angle]
        wiggle.autoreverses = true
        wiggle.duration = randomInterval(0.1, variance: 0.025)
        wiggle.repeatCount = Float.infinity
        layer.add(wiggle, forKey: "wiggle")
        
        let bounce = CAKeyframeAnimation(keyPath: "transform.translation.y")
        bounce.values = [4.0, 0.0]
        bounce.autoreverses = true
        bounce.duration = randomInterval(0.12, variance: 0.025)
        bounce.repeatCount = Float.infinity
        layer.add(bounce, forKey: "bounce")
    }
    
    func stopWiggling() {
        layer.removeAllAnimations()
    }
    
    func randomInterval(_ interval: TimeInterval, variance: Double) -> TimeInterval {
        return interval + variance * Double((Double(arc4random_uniform(1000)) - 500.0) / 500.0)
    }
}
