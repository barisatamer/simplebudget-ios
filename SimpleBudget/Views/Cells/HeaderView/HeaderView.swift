
import UIKit

final class HeaderView: UICollectionViewCell {

    @IBOutlet private weak var nameLabel: UILabel!

    var name: String? {
        get {
            return nameLabel.text
        }
        set {
            nameLabel.text = newValue
        }
    }

}
