//
//  TransactionHistoryCell.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 02/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

class TransactionHistoryCell: UICollectionViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var amount: Double = 0 {
        didSet {
            labelAmount.textColor = amount < 0 ? ColorPalette.Custom.RedLight : ColorPalette.Custom.GreenLighter
            labelAmount.text = "\(abs(amount))"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.addShadowToCard()
    }

}
