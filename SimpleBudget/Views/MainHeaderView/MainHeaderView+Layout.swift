//
//  MainHeaderView+Layout.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 29/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

extension MainHeaderView {
    
    func layoutViews() {
        layoutMonthsParallaxScrollView()
        layoutYearSelectorView()
    }
    
    func layoutMonthsParallaxScrollView() {
        monthsParallaxScrollView.translatesAutoresizingMaskIntoConstraints = false
        monthsParallaxScrollView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        monthsParallaxScrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        monthsParallaxScrollView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        monthsParallaxScrollView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    func layoutYearSelectorView() {
        yearSelectorView.translatesAutoresizingMaskIntoConstraints = false
        yearSelectorView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        yearSelectorViewBottomConstraint = yearSelectorView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -66)
        yearSelectorViewBottomConstraint?.isActive = true
        
        yearSelectorView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        yearSelectorView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
}
