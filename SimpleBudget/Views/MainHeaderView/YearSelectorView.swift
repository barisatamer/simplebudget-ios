//
//  YearSelectorView.swift
//  YearSelector
//
//  Created by Baris Atamer on 25/04/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit


protocol YearSelectorDelegate: class {
    func didPicked(year: Date)
    func didPulled(offsetY: CGFloat)
    func yearScrollViewDidEndDragging(willDecelerate decelerate: Bool)
}

class YearSelectorView: UIView {
    
    var textColor: UIColor = .black
    
    let yearCount = 50
    var years = [Date]()
    var tableViewDragged = false
    
    let tableView: UITableView = {
        let t = UITableView()
        return t
    }()
    
    weak var delegate: YearSelectorDelegate?
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - Private Method
    
    private func commonInit() {
        addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        configure()
    }
    
    private func configure() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        addYears()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tableView.separatorStyle = .none
        tableView.backgroundColor = backgroundColor
        tableView.scrollToRow(at: IndexPath.init(row: years.count - 1, section: 0) , at: UITableViewScrollPosition.bottom, animated: false)
    }
    
    @objc func addYears() {
        years.append(Date().startOfTheYear())
        for _ in 1...yearCount {
            years.append(years.last!.previousYear())
        }
        years.reverse()
        tableView.reloadData()
    }
    
}

extension YearSelectorView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return years.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "YearCell")
        
        let yearStr = years[indexPath.row].stringWithYear()
        cell.textLabel?.font = UIFont.systemFont(ofSize: 45)
        cell.textLabel?.textAlignment = .center
        cell.selectedBackgroundView = UIView()
        cell.textLabel?.text = yearStr
        cell.textLabel?.textColor = self.textColor
        cell.backgroundColor = backgroundColor
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension YearSelectorView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedDate = years[indexPath.row]
        delegate?.didPicked(year: selectedDate)
    }
}

extension YearSelectorView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard tableViewDragged else { return }
        
        let offsetY = scrollView.contentSize.height - scrollView.contentOffset.y - tableView.frame.height
        if offsetY < 0 {
            delegate?.didPulled(offsetY: offsetY)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        tableViewDragged = true
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        delegate?.yearScrollViewDidEndDragging(willDecelerate: decelerate)
        tableViewDragged = false
    }
}
