//
//  MainHeaderView.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 13/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit
import ParallaxScrollView

protocol HeaderViewDelegate: class {
    func didPicked(year: Date)
}

class MainHeaderView: UIView {

    let monthsParallaxScrollView: ParallaxScrollView = {
        let v = ParallaxScrollView()
        return v
    }()
    
    let yearSelectorView: YearSelectorView = {
        let v = YearSelectorView()
        return v
    }()
    
    lazy var months: [String] = {
        return arrayOfMonths()
    }()

    var panGestureRecognizer: UIPanGestureRecognizer?
    var initialHeight: CGFloat = 128
    var isOpened = false
    var isAnimating = false
    var pullMax: CGFloat = 150
    weak var delegate: HeaderViewDelegate?
    
    let fullSize = UIScreen.main.bounds.height - 20
    
    var yearSelectorViewBottomConstraint: NSLayoutConstraint?
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    // MARK: - Private Methods
    
    private func commonInit() {
        addSubview(monthsParallaxScrollView)
        addSubview(yearSelectorView)
        addBottomBorder(.white, thickness: 2)
        
        configure()
    }
    
    private func configure() {
        layoutViews()
        
        // Add pan gesture recognizer
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)) )
        addGestureRecognizer(panGestureRecognizer!)
        
        addMonths()
        
        // Configure year selector
        yearSelectorView.backgroundColor = #colorLiteral(red: 0.2784313725, green: 0.3882352941, blue: 0.4823529412, alpha: 1)
        yearSelectorView.textColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
        yearSelectorView.delegate = self
    }
    
    func addMonths() {
        for i in 0..<months.count {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 24)
            label.text = months[i]
            label.textColor = .white
            monthsParallaxScrollView.addSubviewAsParallax(label)
        }
    }
    
    func arrayOfMonths() -> [String] {
        let formatter = DateFormatter()
        return formatter.shortMonthSymbols
    }
    
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
        guard !isAnimating else { return }
        let translation = panGesture.translation(in: self)
        
        
        if panGesture.state == .began {
            initialHeight = heightConstraint().constant
        } else if panGesture.state == .changed {
            setHeightConstraint(constant: initialHeight + translation.y)
        } else if panGesture.state == .ended && !isOpened {
            endDragging()
        }
    }
    
    func animate() {
        guard !isAnimating else { return }
        isAnimating = true
        
        heightConstraint().constant = fullSize
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 10, options: [], animations: {
            self.superview!.layoutIfNeeded()
        }) { (finished) in
            self.isOpened = true
            
            if finished {
                UIView.animate(withDuration: 1.0) { self.monthsParallaxScrollView.alpha = 0 }
                self.isAnimating = false
            }
        }
    }
    
    func close(withAnimation: Bool = true) {
        guard !isAnimating else { return }
        
        heightConstraint().constant = 66
        yearSelectorViewBottomConstraint?.constant = -66
        
        UIView.animate(withDuration: 1.0) { self.monthsParallaxScrollView.alpha = 1 }
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10, options: [.allowUserInteraction], animations: {
            self.superview!.layoutIfNeeded()
        }) { _ in
            self.isOpened = false
        }
    }
    
    func setHeightConstraint(constant: CGFloat) {
        guard !isAnimating else { return }
        
        heightConstraint().constant = constant
        
        
        let diff = constant - 132
        if diff <= 0  {
            yearSelectorViewBottomConstraint?.constant = constant - 132
        }
    }
    
    func endDragging() {
        if heightConstraint().constant > pullMax {
            animate()
        } else {
            close()
        }
    }
    
}

extension MainHeaderView {
    func heightConstraint() -> NSLayoutConstraint! {
        for constraint in self.constraints {
            if constraint.firstAttribute == .height {
                return constraint
            }
        }
        return nil
    }
}

extension MainHeaderView: YearSelectorDelegate {
    
    func didPicked(year: Date) {
        close()
        delegate?.didPicked(year: year)
    }
    
    func didPulled(offsetY: CGFloat) {
        guard isOpened else { return }
        
        heightConstraint().constant += offsetY
    }
    
    func yearScrollViewDidEndDragging(willDecelerate decelerate: Bool) {
        
        if fullSize - heightConstraint().constant > 100 {
            close()
        } else {
            animate()
        }
    }
}
