//
//  Storyboard.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 01/02/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import Foundation
import UIKit

enum Storyboard: String {
    case Main, Login, Profile
    
    // Components
    case WebView
    
    // Only for STAGE
    case ComingSoon
    
    func instantiate<VC: UIViewController>(_ viewController: VC.Type) -> VC {
        guard
            let vc = UIStoryboard(name: self.rawValue, bundle: nil)
                .instantiateViewController(withIdentifier: VC.storyboardIdentifier) as? VC
            else { fatalError("Couldn't instantiate \(VC.storyboardIdentifier) from \(self.rawValue)") }
        return vc
    }
    
}
