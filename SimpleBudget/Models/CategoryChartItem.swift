//
//  ChartItem.swift
//  SimpleBudget
//
//  Created by Baris Atamer on 27/04/2018.
//  Copyright © 2018 Baris Atamer. All rights reserved.
//

import UIKit

struct CategoryChartItem {
    var categoryName: String
    var totalAmount: Double
    var icon: UIImage?
}
