//
//  OffsetView.swift
//  OffsetView
//
//  Created by Baris Atamer on 18/04/2018.
//

import UIKit

open class OffsetView: UIView {
    
    let label: UIView = {
        let l = UIView()
        return l
    }()

    public var leftPadding: CGFloat = 16 {
        didSet {
            leftConstraint.constant = leftPadding
        }
    }
    
    var leftConstraint: NSLayoutConstraint!
    var lastX: CGFloat = 0
    
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    convenience public init(view: UIView) {
        self.init()
        
        label.addSubview(view)
        
        // Add constraints
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: label.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: label.bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: label.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: label.rightAnchor).isActive = true
    }
    
    
    // MARK: - Private Method
    
    private func commonInit() {
        addSubview(label)
        configure()
    }
    
    private func configure() {
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        leftConstraint = label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: leftPadding)
        leftConstraint.isActive = true
    }
    
    func didScroll(amount: CGFloat, index: Int) {
        let minX = CGFloat(index) * bounds.width - amount
        let maxX = CGFloat(index + 1) * bounds.width - amount
        
        // Disappearing
        if maxX > 0 && maxX < bounds.width  {
            let ratio = abs(minX / bounds.width)
            let movement: CGFloat = bounds.width - label.bounds.width - leftPadding
            leftConstraint.constant = leftPadding + movement * ratio
        } else {
            leftConstraint.constant = leftPadding
        }
        
        lastX = amount
    }
}
