//
//  ParallaxScrollView.swift
//  ParallaxScrollView
//
//  Created by Baris Atamer on 18/04/2018.
//


import UIKit


public protocol ParallaxScrollViewDelegate: class {
    func pageIndexDidChanged(index: Int)
    func parallaxScrollViewWillBeginDragging(_ scrollView: UIScrollView)
    func parallaxScrollViewDidEndDecelerating(_ scrollView: UIScrollView)
}

public extension ParallaxScrollViewDelegate {
    func pageIndexDidChanged(index: Int) { }
    func parallaxScrollViewWillBeginDragging(_ scrollView: UIScrollView) { }
    func parallaxScrollViewDidEndDecelerating(_ scrollView: UIScrollView) { }
}

open class ParallaxScrollView: UIView {
    
    open weak var delegate: ParallaxScrollViewDelegate?
    
    let scrollView: UIScrollView = {
        let s = UIScrollView()
        s.showsVerticalScrollIndicator = false
        s.showsHorizontalScrollIndicator = false
        s.isPagingEnabled = true
        s.alwaysBounceVertical = false
        return s
    }()
    
    var subViews: [OffsetView] = []
    var currentIndex: Int = 0 {
        didSet {
            guard oldValue != currentIndex else { return }
            delegate?.pageIndexDidChanged(index: currentIndex)
        }
    }
    
    
    // MARK: - Initializer
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    // MARK: - Private Method
    
    private func commonInit() {
        addSubview(scrollView)
        configure()
    }
    
    private func configure() {
        scrollView.delegate = self

        // Add constraints
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    open func addSubviewAsParallax(_ view: UIView) {
        let offsetView = OffsetView.init(view: view)
        scrollView.addSubview(offsetView)
        subViews.append(offsetView)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        let width: CGFloat = bounds.width
        
        for (index, item) in subViews.enumerated() {
            item.frame = CGRect(x: CGFloat(index) * width, y: 0, width: width, height: bounds.height)
        }
        
        scrollView.contentSize = CGSize(width: CGFloat(subViews.count) * width,
                                        height: bounds.height)
        
        scrollTo(index: currentIndex, animated: false)
    }
    
    public func setContentOffset(offset: CGPoint) {
        scrollView.contentOffset = offset
    }
    
    public func scrollTo(index: Int, animated: Bool = true) {
        let targetRect = CGRect(x: bounds.width * CGFloat(index),
                                y: 0,
                                width: bounds.width,
                                height: bounds.height)
        
        scrollView.scrollRectToVisible(targetRect, animated: animated)
    }
    
    func calculatePageIndex() -> Int {
        let pageWidth = scrollView.frame.size.width
        let fractionalPage = scrollView.contentOffset.x / pageWidth
        let page = round(fractionalPage)
        return Int(page)
    }
}


// MARK: - UIScrollViewDelegate

extension ParallaxScrollView: UIScrollViewDelegate {
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("Begin dragging")
        delegate?.parallaxScrollViewWillBeginDragging(scrollView)
        
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsetX = scrollView.contentOffset.x
        for i in 0..<subViews.count {
            subViews[i].didScroll(amount: contentOffsetX, index: i)
        }
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        currentIndex = calculatePageIndex()
        delegate?.parallaxScrollViewDidEndDecelerating(scrollView)
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        currentIndex = calculatePageIndex()
    }
    
    
}
