//
//  ViewController.swift
//  ParallaxScrollView
//
//  Created by barisatamer on 04/18/2018.
//  Copyright (c) 2018 barisatamer. All rights reserved.
//

import UIKit
import ParallaxScrollView

class ViewController: UIViewController, ParallaxScrollViewDelegate {
    
    @IBOutlet weak var monthsScrollView: ParallaxScrollView!
    @IBOutlet weak var labelSelectedMonth: UILabel!
    
    lazy var months: [String] = {
        return arrayOfMonths()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    private func configure() {
        monthsScrollView.delegate = self
        addSubviews()
        
        perform(#selector(delayedScroll), with: nil, afterDelay: 1)
    }

    @objc func delayedScroll() {
        monthsScrollView.scrollTo(index: 1, animated: true)
    }
    
    func addSubviews() {
        for i in 0..<months.count {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 24)
            label.text = months[i]
            monthsScrollView.addSubviewAsParallax(label)
        }
    }
    
    func arrayOfMonths() -> [String] {
        let formatter = DateFormatter()
        return formatter.shortMonthSymbols
    }
    
    func pageIndexDidChanged(index: Int) {
        labelSelectedMonth.text = months[index]
    }
    
    func parallaxScrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("Begin dragging")
    }
    
    func parallaxScrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("End decelarating")
    }
}

