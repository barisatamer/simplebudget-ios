# ParallaxScrollView

[![CI Status](http://img.shields.io/travis/barisatamer/ParallaxScrollView.svg?style=flat)](https://travis-ci.org/barisatamer/ParallaxScrollView)
[![Version](https://img.shields.io/cocoapods/v/ParallaxScrollView.svg?style=flat)](http://cocoapods.org/pods/ParallaxScrollView)
[![License](https://img.shields.io/cocoapods/l/ParallaxScrollView.svg?style=flat)](http://cocoapods.org/pods/ParallaxScrollView)
[![Platform](https://img.shields.io/cocoapods/p/ParallaxScrollView.svg?style=flat)](http://cocoapods.org/pods/ParallaxScrollView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ParallaxScrollView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ParallaxScrollView'
```

## Author

barisatamer, baris@igenius.net

## License

ParallaxScrollView is available under the MIT license. See the LICENSE file for more info.
