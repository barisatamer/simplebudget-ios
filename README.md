# Simple Budget iOS


[![Screen recording](https://i.imgur.com/zA81v0R.png)](https://youtu.be/sqLsdD8AuZY   "Screen recording")

A budget tracking app


## Requirements

- iOS 9.3+
- Xcode 9.0+
- Swift 4.0+


## Dependencies

* GRDB.swift
* RxGRDB
* IGListKit
* Charts
* SwiftyBeaver
* IQKeyboardManagerSwift
* ViewAnimator